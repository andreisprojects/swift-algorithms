import UIKit

//loop
func factorial(value: UInt) -> UInt {
    if value == 0 {
        return 1
    }
    var product: UInt = 1
    for i in 1...value {
        product = product * i
    }
    return product
}

print(factorial(value: 0))

//recursion
func recursiveFactorial(value: UInt) -> UInt {
    if value == 0 {
        return 1
    }
//    print("value: \(value)")
    return value * recursiveFactorial(value: value - 1)
}

print(recursiveFactorial(value: 5))
