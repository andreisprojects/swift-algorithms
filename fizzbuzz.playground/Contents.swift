import UIKit

let numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

//fizzbuzz - if else
for n in numbers {
    if n % 3 == 0 && n % 5 == 0 {
        print("\(n) - fizzbuzz")
    } else if n % 3 == 0 {
        print("\(n) - buzz")
    } else if n % 5 == 0 {
        print("\(n) - buzz")
    } else {
        print(n)
    }
}

//improvements:
// n % 3 == 0 && n % 5 == 0 replace with n % 15 == 0

var items = [Int]()
for i in 1...100 {
    items.append(i)
}

for i in items {
    if i % 15 == 0 {
        print("\(i) - fizzbuzz")
    } else if i % 3 == 0 {
        print("\(i) - buzz")
    } else if i % 5 == 0 {
        print("\(i) - buzz")
    } else {
        print(i)
    }
}


//fizzbuzz - switch

for i in items {
    switch i {
    case let x where (i % 15 == 0):
        print("\(i) - fizzbuzz")
    case let x where (i % 3 == 0):
        print("\(i) - fizz")
    case let x where (i % 5 == 0):
        print("\(i) - buzz")
    default:
        print(i)
    }
}
