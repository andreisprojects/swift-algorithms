import UIKit

// sorted arrays
let numbers = [1,2,4,6,8,9,11,13,16,17,20]

var hundred = [Int]()
for i in 1...100 {
    hundred.append(i)
}

// linear search
func linearSearch(value: Int, array: [Int]) -> Bool {
    for n in array {
        if n == value {
            return true
        }
    }
    return false
}

let linearResult = linearSearch(value: 5, array: numbers)
print("linear numbers result:", linearResult)


// binary search (sorted array)

func binarySearch(value: Int, array: [Int]) -> Bool {
    var leftIndex = 0
    var rightIndex = array.count - 1
    
    while leftIndex <= rightIndex {
        let middleIndex = (leftIndex + rightIndex) / 2
        let middleValue = array[middleIndex]
        
        print("mid value \(middleValue), left index \(leftIndex), right index \(rightIndex), [\(array[leftIndex]), \(array[rightIndex])]")
        
        if middleValue == value {
            return true
        }
        
        if value < middleValue {
            rightIndex = middleIndex - 1
        }
        
        if value > middleValue {
            leftIndex = middleIndex + 1
        }
    }
    return false
}

let binaryResult = binarySearch(value: 18, array: numbers)
print("binary numbers result:", binaryResult)

let binaryHundredResult = binarySearch(value: 18, array: hundred)
print("binary hundred result:", binaryHundredResult)
